package rooms;
import enemies.Enemy;
import items.Item;

import java.util.ArrayList;
import java.util.HashMap;

import characters.Person;



public class RoomImpl implements Room {
	private Room north, south, east, west;
	private HashMap<Boolean, String> info;
	private String directions;
	private ArrayList<Item> roomItems;
	private ArrayList<Person> roomPeople;
	private Enemy enemy;

	/*
	 *	Make sure that your Room info includes brief descriptions of what lies in
	 *	each of the four compass directions.
	 */

	public RoomImpl() {
		info = new HashMap<Boolean, String>();
		roomItems = new ArrayList<Item>();
		roomPeople = new ArrayList<Person>();

		north = null;
		south = null;
		east = null;
		west = null;
		enemy = null;
	}

	public RoomImpl(Room n, Room s, Room e, Room w) {
		info = new HashMap<Boolean, String>();
		roomItems = new ArrayList<Item>();
		roomPeople = new ArrayList<Person>();
		
		north = n;
		south = s;
		east = e;
		west = w;

		enemy = null;
	}

	@Override
	public void printInfo() {
		for (boolean b : info.keySet()) {
			if (b)
				System.out.println(info.get(b));
		}
		
		/* Show items in the room. */
		for (Item i : roomItems) {
			if (!i.isHidden()) {
				char fst = i.getName().charAt(0);
				if (fst == 'a' || fst == 'e' || fst == 'i' || fst == 'o' || fst == 'u')
					System.out.println("You see an " + i.getName() + ".");
				else
					System.out.println("You see a " + i.getName() + ".");
			}
		}
		
		System.out.println();
		System.out.println(directions);
	}

	@Override
	public void addInfo(boolean isTrue, String infoIn) {
		info.put(isTrue,  infoIn);
	}
	
	@Override
	public void setDirections(String directionsIn) {
		directions = directionsIn;
	}
	
	public ArrayList<Item> getItems() {
		return roomItems;
	}
	
	public boolean hasItem(Item itemIn) {
		for (Item i : roomItems) {
			if (itemIn.equals(i))
				return true;
		}
		return false;
	}
	
	@Override
	public void putItem(Item itemIn) {
		roomItems.add(itemIn);
	}
	
	@Override
	public void putPerson(Person personIn) {
		roomPeople.add(personIn);
	}
	
	@Override
	public ArrayList<Person> getPeople() {
		return roomPeople;
	}
	
	public Item removeItem(Item itemIn) {
		Item item = itemIn;
		roomItems.remove(item);
		return item;
	}
	
	public Enemy getEnemy() {	// Public Enemy (lol)
		return enemy;
	}
	
	public void putEnemy(Enemy enemyIn) {
		enemy = enemyIn;
	}
	
	public void clearEnemy() {
		enemy = null;
	}
	
	@Override
	public Room north() {
		return north;
	}

	@Override
	public Room south() {
		return south;
	}

	@Override
	public Room east() {
		return east;
	}

	@Override
	public Room west() {
		return west;
	}

	@Override
	public void attachRoom(Room room, char direction) throws NullPointerException {
		switch (direction) {
		case 'n':
			north = room;
			break;
			
		case 's':
			south = room;
			break;
			
		case 'e':
			east = room;
			break;
			
		case 'w':
			west= room;
			break;
			
		default:
			throw new NullPointerException("ERROR: Invalid room direction entered");
		}
	}
}

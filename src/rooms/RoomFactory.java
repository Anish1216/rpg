package rooms;


public class RoomFactory {
	
	public static Room build() {
		return new RoomImpl();
	}
	
	public static Room build(Room source, char direction) {
		Room r;
		switch (direction) {
		case 'n':
			
			/* Attach the source room at the SOUTH end. */
			r = new RoomImpl(null, source, null, null);
			source.attachRoom(r, direction);
			return r;
		
		case 's':
			r = new RoomImpl(source, null, null, null);
			source.attachRoom(r, direction);
			return r;
		
		case 'e':
			r = new RoomImpl(null, null, null, source);
			source.attachRoom(r, direction);
			return r;
		
		case 'w':
			r = new RoomImpl(null, null, source, null);
			source.attachRoom(r, direction);
			return r;
		
		default:
			System.out.println("Not a valid room.");
			return null;
		}
	}

}

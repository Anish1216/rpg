package rooms;

public interface Door {
	
	public boolean isLocked();
	public void unlock();
}

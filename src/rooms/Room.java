package rooms;
import items.Item;

import java.util.ArrayList;

import characters.Person;
import enemies.Enemy;

public interface Room {
	
	public void printInfo();
	public void addInfo(boolean isTrue, String info);
	
	public Room north();
	public Room south();
	public Room east();
	public Room west();
	
	public void attachRoom(Room room, char direction) throws NullPointerException;
	public void putItem(Item item);
	public void putPerson(Person person);
	
	public ArrayList<Person> getPeople();
	public ArrayList<Item> getItems();
	public boolean hasItem(Item item);
	public Item removeItem(Item item);
	
	public Enemy getEnemy();
	public void putEnemy(Enemy enemy);
	public void setDirections(String directions);
}

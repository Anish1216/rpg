package levels;
import characters.Person;
import characters.PersonFactory;
import items.Bag1A;
import items.Item;
import player.Console;
import player.Player;
import enemies.EnemyFactory;
import enemies.Guard;
import rooms.Room;
import rooms.RoomFactory;

public class TestStage {

	public static void main(String[] args) {
		
		/* Load player and console. */
		Player player = Player.getInstance();
		Console console = Console.getInstance();
		
		/* Create needed items. You have to set the
		 * name and description for each item. */
		Item bag = new Bag1A();
		
		/* Create character(s). */
		Person prisoner = PersonFactory.build("prisoner1a");
		
		/* Create enemy. */
		Guard g1 = (Guard) EnemyFactory.build("guard");
		
		/* Create rooms. */
		Room r1, r2, r3;
		r1 = RoomFactory.build();
		r2 = RoomFactory.build(r1, 'n');
		r3 = RoomFactory.build(r2, 'e');
		
		/* Place items/people in rooms. */
		r1.putItem(bag);
		r2.putPerson(prisoner);
		
		/* Set room info. */
		r1.addInfo(true, "This is the first room.");
		r1.setDirections("NORTH: There is a door.");
		
		r2.addInfo(true, "This is the second room. There is a prisoner sitting on the ground.");
		r2.setDirections("SOUTH: There is a door.\nEAST: There is a hole in the wall.");
		
		r3.addInfo(true, "This is the third room.");
		r3.setDirections("WEST: There is a hole in the wall.");
		r3.putEnemy(g1);
		
		player.setRoom(r1);
		
		System.out.println("You are in a dungeon.\nThere are no windows, no guards, and the place looks empty.\n");
		
		/* This must go at the very end. */
		console.run();
	}
}

package levels;

public class Stage {
	
	private Stage next;
	
	public Stage() {
		next = null;
	}
	
	public Stage append(Stage s) {
		s = new Stage();
		this.next = s;
		return s;
	}
	
	public Stage getNext() {
		return next;
	}
}

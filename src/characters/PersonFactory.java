package characters;

public class PersonFactory {

	public static Person build(String personName) {
		switch (personName) {
		case "prisoner1a":
			return new Prisoner1A();

		default:
			return null;
		}
	}
}
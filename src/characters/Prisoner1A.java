package characters;

public class Prisoner1A implements Person {
	
	private String name;
	private String description;
	private String[] conversation;
	private int state;
	
	public Prisoner1A() {
		conversation = new String[2];
		name = "prisoner";
		description = "An old man who has clearly been here for years.";
		state = 0;
		setDialogue();
	}
	
	@Override
	public void setDialogue() {
		StringBuilder sb;
		
		sb = new StringBuilder();
		sb.append("\"So, you're the newcomer, huh?");
		sb.append(" I've been here for as long as I can remember.");
		sb.append(" Last night, some masked intruders dropped by this area.");
		sb.append(" I have no idea what they were doing. Maybe they left something?");
		sb.append(" I think they were here for you. Try and find it.\n");
		conversation[0] = sb.toString();
		
		sb = new StringBuilder();
		sb.append("\"You really should try and find a way to escape.");
		sb.append(" You're not meant to be here.\"");
		conversation[1] = sb.toString();
	}
	
	@Override
	public void talk() {
		System.out.println(conversation[state]);
		state = 1;
	}
	
	@Override
	public void update() {
		state ++;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public String getDescription() {
		return description;
	}

}

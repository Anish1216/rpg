package characters;

public interface Person {
	
	public void setDialogue();
	public void talk();
	public void update();
	
	public String getName();
	public String getDescription();
}

package player;

public class GeneralInterpreter {
	
	private static GeneralInterpreter instance;
	
	private GeneralInterpreter() {}
	
	public static GeneralInterpreter getInstance() {
		if (instance == null) {
			instance = new GeneralInterpreter();
		}
		
		return instance;
	}
	
	public void interpret(Player player, String input) {
		String parsed;
		String[] words;
		String arg;
		parsed = input.toLowerCase();
		words = parsed.split(" ", 2);	// Split string at first space only.
		arg = words[0].trim();


		/*
		 * One-word commands
		 * */

		if (parsed.equals("look")) {
			player.look();
			return;
		}
		
		else if (parsed.equals("inventory")) {
			player.printInv();
			return;
		}


		/*
		 * Two-word commands
		 * */

		if (words[1].length() < 1) {
			System.out.println("Invalid command. You must enter at least one more word.");
			return;
		}

		else if (arg.equals("go"))
			player.go(words[1]);

		else if (arg.equals("get"))
			player.takeItem((words[1]));

		else if (arg.equals("look") && words[1] != null)
			player.lookAt(words[1]);

		else if (arg.equals("talk"))
			player.talkTo(words[1]);
		
		else if (arg.equals("use"))
			player.useItem(words[1]);

		else if (parsed.equals(""))
			System.out.println("You must enter something.");

		else if (!parsed.equals("endgame"))
			System.out.println("Invalid command. Type \"help\" for available commands.");
	}
}

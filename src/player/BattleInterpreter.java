package player;

public class BattleInterpreter {

	private static BattleInterpreter instance;

	private BattleInterpreter() {}

	public static BattleInterpreter getInstance() {
		if (instance == null) {
			instance = new BattleInterpreter();
		}

		return instance;
	}

	public void interpret(Player player, String input) {
		String parsed;
		String[] words;
		String arg;

		parsed = input.toLowerCase();

		/*
		 * Commands
		 * */

		if (parsed.charAt(0) == 'w') {
			Console.setState("weapon");
			System.out.println("Type the name of a weapon or \"punch\" to punch.");
			System.out.println("Weapons:");
		}

		else if (parsed.charAt(0) == 'm') {
			Console.setState("magic");
			System.out.println("Type the name of a spell.");
			System.out.println("Spells:");
		}

		else
			System.out.println("Sorry, but that is not a valid command.");
	}
}

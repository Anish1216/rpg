package player;

public class MagicInterpreter {

	private static MagicInterpreter instance;

	private MagicInterpreter() {}

	public static MagicInterpreter getInstance() {
		if (instance == null) {
			instance = new MagicInterpreter();
		}

		return instance;
	}

	public void interpret(Player player, String input) {
		String parsed;
		String[] words;
		String arg;
		parsed = input.toLowerCase();


		/*
		 * Commands
		 * */

		if (parsed.equals("fire")) {
			System.out.println("[FIRE SPELL]");
			player.getEnemy().subHealth(10);
		}

		else
			System.out.println("Sorry, but that is not a valid spell.");
		
		Console.setState(null);
	}
}

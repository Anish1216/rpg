package player;
import items.Item;
import items.Weapon;
import items.Item.Type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import characters.Person;
import enemies.Enemy;
import rooms.Room;
import rooms.RoomImpl;

/**
 * 
 * @author AnishKrishnan
 *
 */
public class Player {
	
	private static Player instance;	// Singleton
	
	private int health, mana;
	private int xp;		// This is how the player will learn new spells.
	private Room currentRoom;
	private Enemy currentEnemy;
	private Weapon currentWeapon;
	private boolean fighting;
	
	/* The ArrayList will hold all the items; the HashMap will document them. */
	private ArrayList<Item> items;
		
	private Player() {}
	
	public static Player getInstance() {
		if (instance == null) {
			instance = new Player();
			instance.health = 100;
			instance.mana = 100;
			instance.xp = 0;
			instance.fighting = false;
			instance.items = new ArrayList<Item>();
		}
		return instance;
	}
	
	public Player(int h, int m, int l, int e) {
		health = h;
		mana = m;
		xp = e;
		fighting = false;
	}
	
	public void attack(int damage) {
		currentEnemy.subHealth(damage);
	}
	
	public void look() {
		System.out.println("\n");
		currentRoom.printInfo();
	}
	
	/**
	 * This method allows the player to look at whatever is specified.
	 * It works with both items and NPCs.
	 * @param name
	 */
	public void lookAt(String name) {
		
		/* Only print items in the room that are not hidden. */
		for (Item i : getRoom().getItems()) {
			if (i.getName().equals(name) && (!i.isHidden())) {
				System.out.println(i.getDescription());
				return;
			}
		}
		
		for (Person p : getRoom().getPeople()) {
			if (p.getName().equals(name)) {
				System.out.println(p.getDescription());
				return;
			}
		}

		System.out.println("You cannot look at that.");
	}
	
	/**
	 * This method directly adds Items to the inventory. It cannot be called from the console.
	 * @param item
	 */
	public void addItem(Item item) {
		items.add(item);
	}
	
	public void takeItem(String itemName) {
		Item item = null;
		for (Item i : currentRoom.getItems()) {
			if (i.getName().equals(itemName) && !i.isHidden()) {
				item = i;
				break;
			}			
		}
		
		if (item == null) {
			System.out.printf("%s not found.\n", itemName);
			return;
		}
		
		addItem(item);
		currentRoom.removeItem(item);

		System.out.printf("Added %s to the inventory.\n", itemName);
	}
	
	public boolean hasItem(String itemName) {
		if (items.isEmpty())
			return false;
		
		for (Item i: items) {
			if (i.getName().equals(itemName))
				return true;
		}
		
		return false;
	}
	
	public void useItem(String itemName) {
		for (Item i : items) {
			if (i.getName().equals(itemName)) {
				i.use();
				return;		// Only use an item once.
			}
		}
		System.out.printf("You cannot use \"%s\", because you do not have it.\n", itemName);
	}
	
	public void dropItem(String itemName) {
		if (!hasItem(itemName))
			return;
		
		for (Item i : items) {
			if (i.getName().equals(itemName)) {
				items.remove(i);
				return;
			}
		}
	}
	
	public void talkTo(String person) {
		for (Person p : currentRoom.getPeople()) {
			if (p.getName().equals(person)) {
				p.talk();
				return;
			}
		}
		
		System.out.printf("You cannot talk to \"%s.\" Try being more specific.\n", person);
	}

	public void checkEnemy() {	// Void?
		currentEnemy = currentRoom.getEnemy();
		if (currentEnemy != null && currentEnemy.isAlive()) {
			System.out.println("\nYou see an enemy!\nENEMY: " + currentEnemy.getName());
			fighting = true;
			System.out.println("Battle commands:\n(w)eapon\n(m)agic\n(i)tem");
		}
	}
	
	public void die() {
		System.out.println("You have died!");
		System.exit(0);
	}
	
	
	/* 
	 * Movement 
	 * */
	
	public void go(String direction) {
		Room roomTo = null;
		switch (direction) {
		case "north":
			roomTo = currentRoom.north();
			break;
			
		case "south":
			roomTo = currentRoom.south();
			break;
			
		case "east":
			roomTo = currentRoom.east();
			break;
			
		case "west":
			roomTo = currentRoom.west();
			break;
		}
		
		if (roomTo != null) {
			currentRoom = roomTo;
			look();
			checkEnemy();
		}
		else
			System.out.println("You cannot go in that direction.");
	}

	/* 
	 * Statistics 
	 * */
	
	public void printStats() {
		System.out.println("\nSTATS\n----------------");
		System.out.println("Health: " + health);
		System.out.println("Mana: " + mana);
		System.out.println("XP: " + xp);
		
	}
	
	public void printInv() {
		
		HashMap<String, Integer> inventory = new HashMap<String, Integer>();
		for (Item i : items) {
			if (inventory.containsKey(i.getName())) {
				int quantity = inventory.get(i.getName());
				inventory.put(i.getName(), quantity + 1);
			} else {
				inventory.put(i.getName(), 1);
			}
		}
		
		System.out.println("\nINVENTORY\n----------------");
		for (String s : inventory.keySet()) {
			System.out.printf("Item: %s\tQuantity: %d\n", s, inventory.get(s));
		}
		System.out.println("----------------");
	}
	
	/* 
	 * Getters/Setters
	 *  */
	
	public ArrayList<Item> getItems() {
		return items;
	}
	
	public ArrayList<Weapon> getWeapons() {
		ArrayList<Weapon> weapons = new ArrayList<Weapon>();
		for (Item i : items) {
			if (i.getType() == Type.WEAP)
				weapons.add((Weapon) i);
		}
		return weapons;
	}
	
	public int getHealth() {
		return health;
	}
	
	public int getMana() {
		return mana;
	}
	
	public Weapon getWeapon() {
		return currentWeapon;
	}
	
	public Room getRoom() {
		return currentRoom;
	}
	
	public Enemy getEnemy() {
		return currentEnemy;
	}
	
	public void modHealth(int mod) {
		health += mod;	// Make sure mod is negative when need be.
	}
	
	public void modMana(int mod) {
		mana += mod;
	}
	
	public void setRoom(Room roomIn) {
		currentRoom = roomIn;
	}
	
	public boolean inBattle() {
		return fighting;
	}
	
	public void setBattle(boolean fightingIn) {
		fighting = fightingIn;
	}
}

package player;

import items.Item;
import items.Weapon;

import java.util.Scanner;

import enemies.Enemy;

public class Console {
	
	private static Console instance;
	private Scanner in;
	private Player player;
	private static String state;
	
	private Console() {}
	
	public static Console getInstance() {
		if (instance == null) {
			instance = new Console();
			instance.player = Player.getInstance();
		}
		
		return instance;
	}

	public void run() {
		in = new Scanner(System.in);
		String command;

		do {
			System.out.println("\n---- Enter a command. ----");
			command = in.nextLine();
			if (command.length() < 1) {
				System.out.println("Sorry, but you must type something.");
				continue;
			}

			/* Split the interpretation into different classes. */
			if (player.inBattle()) {
				
				if (state == null) {
					BattleInterpreter.getInstance().interpret(player, command);
					continue;
				}
				
				else if (state.equals("weapon")) {
					WeaponInterpreter.getInstance().interpret(player, command);
				}
				
				else if (state.equals("magic")) {
					MagicInterpreter.getInstance().interpret(player, command);
				}
				
				Enemy enemy;
				if ((enemy = player.getEnemy()).isAlive())
					enemy.fight();
			}
			else
				GeneralInterpreter.getInstance().interpret(player, command);
		} while (!command.equals("endgame"));

		in.close();
	}
	
	public static String getState() {
		return state;
	}
	
	public static void setState(String stateIn) {
		state = stateIn;
		if (state == null)
			System.out.println("\nBattle commands:\n(w)eapon\n(m)agic\n(i)tem");
	}
}

package player;

public class WeaponInterpreter {

	private static WeaponInterpreter instance;

	private WeaponInterpreter() {}

	public static WeaponInterpreter getInstance() {
		if (instance == null) {
			instance = new WeaponInterpreter();
		}

		return instance;
	}

	public void interpret(Player player, String input) {
		String parsed;
		String[] words;
		String arg;
		parsed = input.toLowerCase();


		/*
		 * Commands
		 * */

		if (parsed.equals("punch")) {
			System.out.println("[PUNCHING]");
			player.getEnemy().subHealth(10);
		}

		else
			System.out.println("Sorry, but that is not a valid weapon.");
		
		Console.setState(null);
	}
}

package enemies;

public class EnemyFactory {
	
	public static Enemy build(String enemyName) {
		if (enemyName.equals("guard"))
			return new Guard();
		
		return null;
	}
}

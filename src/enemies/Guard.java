package enemies;
import java.util.ArrayList;

import player.Player;

public class Guard implements Enemy {
	
	private String name;
	private int health, agility, xpToPlayer;
	private boolean alive;
	private ArrayList<Attack> attacks;
	private Player player;
	
	public Guard() {
		name = "Guard";
		health = 20;
		agility = 5;
		xpToPlayer = 10;
		alive = true;
		
		attacks = new ArrayList<Attack>();
		player = Player.getInstance();
	}
	
	@Override
	public void initialize() {
		attacks.add(new Attack("Slash", 10, 0.8));
		attacks.add(new Attack("Stab", 15, 0.6));
	}
	
	@Override
	public void fight() {
		System.out.println("Guard did 5 damage!");
		player.modHealth(-5);
	}
	
	@Override
	public boolean isAlive() {
		return alive;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getHealth() {
		return health;
	}

	@Override
	public int getAgility() {
		return agility;
	}

	@Override
	public int getXP() {
		return xpToPlayer;
	}

	@Override
	public void subHealth(int mod) {
		health -= mod;
		if (health <= 0)
			die();
	}

	@Override
	public void die() {
		alive = false;
		System.out.println("You have defeated the Guard!");
		player.setBattle(false);
	}
}

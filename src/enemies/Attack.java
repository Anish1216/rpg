package enemies;
public class Attack {
	
	public String name;
	public int damage;
	public double chance;
	
	public Attack(String name, int damage, double chance) {
		this.name = name;
		this.damage = damage;
		this.chance = chance;
	}
}

package enemies;
public interface Enemy {

	public void initialize();
	public void fight();
	public boolean isAlive();
	public String getName();
	public int getHealth();
	public int getAgility();
	public int getXP();
	public void subHealth(int mod);
	public void die();
}

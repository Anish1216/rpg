package items;

public abstract class Item {
	
	private String name, description;
	private boolean hidden;
	
	public enum Type {
		MISC,
		WEAP,
		POTN,
	}
	
	private Type type;
	
	public abstract void use();
	public abstract void update(int n);
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public boolean isHidden() {
		return hidden;
	}
	
	public Type getType() {
		return type;
	}
	
	public void setName(String nameIn) {
		name = nameIn;
	}
	
	public void setDescription(String descriptionIn) {
		description = descriptionIn;
	}
	
	public void setHidden(boolean state) {
		hidden = state;
	}
}

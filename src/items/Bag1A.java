package items;

import player.Player;

public class Bag1A extends Item {
	
	private String desc;
	Key key;
	Player player;
	
	public Bag1A() {
		setName("bag");
		desc = "A dirty cloth bag. It seems to contain something.";
		setDescription(desc);
		setHidden(false);
		key = KeyFactory.build("key1a");
		player = Player.getInstance();
	}

	@Override
	public void use() {
		player.addItem(key);
		System.out.println("You open the bag and find an old key.");
		player.dropItem("bag");
	}

	@Override
	public void update(int n) {
		// TODO Auto-generated method stub
		
	}

}

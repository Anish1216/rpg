package items;

public class KeyFactory {

	public static Key build(String keyName) {
		switch (keyName) {
		case "key1a":
			return new Key1A();

		default:
			return null;
		}
	}
}

package items;

import rooms.Door;

public abstract class Key extends Item {
	
	public abstract void unlock(Door door);

}

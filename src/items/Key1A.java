package items;

import rooms.Door;

public class Key1A extends Key {
	
	private String desc;
	
	public Key1A() {
		setName("prison key");
		desc = "A rusty bronze key. It smells awful.";
		setDescription(desc);
		setHidden(true);
	}
	
	@Override
	public void use() {
		System.out.println("You can't use that here.");
	}
	
	@Override
	public void unlock(Door door) {
		// TODO Auto-generated method stub
	}

	@Override
	public void update(int n) {
		// TODO Auto-generated method stub
	}
}

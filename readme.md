# (Unnamed) RPG

## Authors: Anish Krishnan, Jocelynne Gonzales

## Description
This is a turn-based RPG (role-playing game) programmed in Java. It is an entirely original project, and implements multiple design patterns and data structures seen in object-oriented programming. In this game, the player character escapes a dungeon and collects items in a quest to become king/queen.

### Current features
Although the game is far from complete, some of the current features include:
- The ability to navigate between rooms
- The ability to pick up, carry, and use items
- The ability to talk to NPCs
- The ability to engage an enemy in combat

### Planned features
Further features are planned; some are already in early stages of production. These include:
- The ability to use spells and potions
- Conversation threads (which will likely use JSON)
- XP and leveling up

### Notes
As of now, the game is far from complete. However, we are in the process of adding features. If you would like to be a part of this project, feel free to contact us.